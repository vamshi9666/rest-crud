const express = require("express")
const router = express.Router()


// get all users
router.get("/",(req, res ) => {
    const data = require('../data/users.json')
    res.status(200).json(data)
})


// get one user
router.get("/:id", (req,res) => {
    try {
        const data  = require("../data/users.json")
        const { params } = req
        const id = params.id
        console.log("id is ", id)
        const oneUser = data.find(user => user.id === parseInt(id) )
    
    
        res.status(200).json({
            user: oneUser
        })    
    } catch (error) {
        res.status(500).json({
            error: error
        })
    }
    
})

// adds data to json  
router.post('/', (req,res) => {
    const body= req.body
    console.log(" body is ", req.body)
    res.status(200).json({
        reqBody: body
    })
})

module.exports = router
const express = require("express")
const router = express.Router()

router.get("/", (req, res) => {
    const data = require("./../data/posts.json")
    res.status(200).json(data)
})

router.get("/onepost/:id", (req,res)=> {
    res.status(200).json({
        post: {
            name: "new post"
        }
    })
})

router.post('/addpost', (req,res) =>{
    let   isFormValid = false
    if (true){
        isFormValid  = true
    }
    

    if (isFormValid){
        res.status(200).json({
            message: "added post",
            error: false
        })
    }
    else {
        res.status(400).json({
            message:"input invalid",
            error : true
        })
    }
})


module.exports = router
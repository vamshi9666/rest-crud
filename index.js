const express = require("express")
const bodyparser = require("body-parser")
const app = express()
const dotenv = require("dotenv")
const userRoutes = require("./routes/users")
const postRoutes = require("./routes/posts")
const logger = require("./middlewares/logger")

dotenv.config()

app.use(bodyparser.urlencoded({ extended: false })); //parse simple URL encoded data
app.use(bodyparser.json())
app.use(logger)



app.use('/users',userRoutes)
app.use('/posts',postRoutes)

const PORT = process.env.PORT;

app.listen(PORT, () => {
    console.log("server running at http://locahost:"+PORT)
})


//express 
//body-parser
//dotenv

